import { createWebHistory, createRouter } from "vue-router";
import { store } from "./store";

const routes = [
    {
        path: "/",
        name: "Home",
        component: () => import("./views/Home.vue"),
    },
    {
        path: "/login",
        name: "Login",
        component: () => import("./views/Login.vue"),
    },
    {
        path: "/checkout",
        name: "Checkout",
        component: () => import("./views/Checkout.vue"),
        meta: {
            layout: "checkout",
        },
    },
    {
        path: "/admin",
        component: () => import("./views/Admin/index.vue"),
        meta: {
            layout: "admin",
            requiresAuth: true,
            requiresAdmin: true,
        },
        children: [
            {
                path: "",
                component: () => import("./views/Admin/Dashboard.vue"),
            },
            {
                path: "dashboard",
                component: () => import("./views/Admin/Dashboard.vue"),
            },
        ],
    },
    {
        path: "/:pathMatch(.*)*",
        component: () => import("./views/PageNotFound.vue"),
        meta: {
            layout: "empty",
        },
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    const isLoggedIn = store.getters["isAuthenticated"];
    // Role getters
    const isAdmin = store.getters["isAdmin"];
    if (to.name == "Login" && isLoggedIn) {
        next("/");
    }
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, display guest greeting.

        if (!isLoggedIn) {
            next({
                path: "/login",
            });
        } else {
            //if only admin
            if (
                to.matched.some((record) => record.meta.requiresAdmin) &&
                isAdmin
            ) {
                next();
            } else {
                //if logged in and not an admin
                next("/");
            }
        }
    } else {
        next(); // make sure to always call next()!
    }
});

export default router;
