import { createStore } from "vuex";
import VuexPersistence from "vuex-persist";

// Create a new store instance.
export const store = createStore({
    state() {
        return {
            cart: [],
            user: null,
            token: null,
        };
    },
    getters: {
        getUser(state) {
            return state.user;
        },
        isAuthenticated(state) {
            return state.token ? true : false;
        },
        isAdmin(state) {
            return state.user ? state.user.isAdmin : null;
        },
        getCartItems(state) {
            return state.cart;
        },
        getCartLength(state) {
            return state.cart.length;
        },
        getCartTotalPrice(state) {
            let total = 0;
            state.cart.map((Food) => {
                total += Food.price * Food.quantity;
            });
            return total;
        },
    },
    mutations: {
        SET_USER(state, payload) {
            state.user = payload.user;
            state.token = payload.access_token;
        },
        pushFoodToCart(state, Food) {
            Food.quantity = 1;
            state.cart.push(Food);
        },
        incrementFoodItem(state, CartFood) {
            CartFood.quantity++;
        },
        decrementFoodItem(state, CartFood) {
            CartFood.quantity--;
            if (CartFood.quantity == 0) {
                //remove if quantity is 0
                let indexOffood = state.cart.indexOf(CartFood);
                state.cart.splice(indexOffood, 1);
            }
        },

        removeFood(state, food) {
            let indexOffood = state.cart.indexOf(food);
            state.cart.splice(indexOffood, 1);
        },
    },
    actions: {
        setUser({ state, commit }, payload) {
            commit("SET_USER", payload);
        },

        addFoodToCart({ state, commit }, Food) {
            const cartFood = state.cart.find((prod) => prod.id === Food.id);
            if (!cartFood) {
                commit("pushFoodToCart", Food);
            } else {
                //show already added message
            }
        },

        incrementFoodItem({ state, commit }, Food) {
            const cartFood = state.cart.find((prod) => prod.id === Food.id);
            if (cartFood) {
                commit("incrementFoodItem", cartFood);
            }
        },
        decrementFoodItem({ state, commit }, Food) {
            const cartFood = state.cart.find((prod) => prod.id === Food.id);
            if (cartFood) {
                commit("decrementFoodItem", cartFood);
            }
        },
        removeFoodFromCart({ state, commit }, Food) {
            commit("removeFood", Food);
        },
    },
    plugins: [new VuexPersistence().plugin],
});
