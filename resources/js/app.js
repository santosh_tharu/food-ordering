require("./bootstrap");
import { createApp } from "vue";

import App from "./App.vue";
import router from "./router";
import { store } from "./store";

import Toast from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import "animate.css";
const app = createApp(App);
const options = {
    transition: {
        enter: "animate__animated animate__fadeInDown",
        leave: "animate__animated animate__fadeOutUp",
    },
};

app.use(Toast, options);
app.use(VueSweetalert2);
app.mixin({
    methods: {
        confirmSwal(title, icon, confirmText) {
            return this.$swal.fire({
                customClass: {
                    confirmButton: "btn btn-success",
                    cancelButton: "btn btn-danger",
                },
                title: title,
                icon: icon,
                showCancelButton: true,
                confirmButtonText: confirmText ? confirmText : "Ok",
                showClass: {
                    popup: "animate__animated animate__fadeInDown",
                },
                hideClass: {
                    popup: "animate__animated animate__fadeOutUp",
                },
            });
        },
    },
});

app.use(router);
app.use(store);

app.mount("#app");
