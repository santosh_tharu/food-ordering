"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_Login_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/views/Login.vue?vue&type=script&lang=js":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/views/Login.vue?vue&type=script&lang=js ***!
  \******************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm-bundler.js");
/* harmony import */ var _components_partials_Navbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/partials/Navbar */ "./resources/js/components/partials/Navbar.vue");
/* harmony import */ var vue_toastification__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-toastification */ "./node_modules/vue-toastification/dist/index.mjs");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    Navbar: _components_partials_Navbar__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  setup: function setup() {
    // Get toast interface
    var toast = (0,vue_toastification__WEBPACK_IMPORTED_MODULE_1__.useToast)();
    return {
      toast: toast
    };
  },
  data: function data() {
    return {
      form: {
        login: {
          email: "",
          password: ""
        },
        register: {
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          password_confirmation: ""
        }
      },
      errorMessage: {
        login: {
          email: "",
          password: ""
        },
        register: {
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          password_confirmation: ""
        }
      },
      hideLoginPassword: true,
      hideRegisterPassword: true,
      hideRegisterpassword_confirmation: true
    };
  },
  methods: _objectSpread(_objectSpread({}, (0,vuex__WEBPACK_IMPORTED_MODULE_2__.mapActions)(["setUser"])), {}, {
    loginUser: function loginUser() {
      var _this = this;

      var isEmpty = Object.values(this.errorMessage.login).every(function (x) {
        return x === "";
      }); //no validation error

      if (isEmpty) {
        this.logging = true;
        axios.post("api/login", this.form.login).then(function (res) {
          _this.setUser(res.data);

          _this.toast.success("User logged in successfully");

          _this.clearForm();

          if (_this.$route.query.returnUrl) {
            _this.$router.push({
              path: _this.$route.query.returnUrl
            });
          } else {
            _this.$router.push("/");
          }
        })["catch"](function (err) {
          if (err) {
            var message = err.response.data.errors.message;

            _this.toast.error(message);
          }
        });
      }
    },
    registerUser: function registerUser() {
      var _this2 = this;

      var isEmpty = Object.values(this.errorMessage.register).every(function (x) {
        return x === "";
      }); //no validation error

      if (isEmpty) {
        axios.post("api/register", this.form.register).then(function (res) {
          _this2.toast.success(res.data.message);

          _this2.flipForm();

          _this2.clearForm();
        })["catch"](function (err) {
          var errors = err.response.data.errors;

          if (errors.email) {
            _this2.toast.error(errors.email[0]);
          }
        });
      }
    },
    clearForm: function clearForm() {
      this.form = {
        login: {
          email: "",
          password: ""
        },
        register: {
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          password_confirmation: ""
        }
      };
    },
    flipForm: function flipForm() {
      document.querySelector("#flipper").classList.toggle("flip");
    },
    validateEmail: function validateEmail(type) {
      var email = "";
      var errorMessage = "";

      if (type == "login") {
        email = this.form.login.email;
      } else {
        email = this.form.register.email;
      }

      if (!String(email).toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
        errorMessage = "Please enter a valid email address";
      } else {
        errorMessage = "";
      }

      if (type == "login") {
        this.errorMessage.login.email = errorMessage;
      } else {
        this.errorMessage.register.email = errorMessage;
      }
    },
    validatePassword: function validatePassword(type) {
      var password = "";
      var errorMessage = "";

      if (type == "login") {
        password = this.form.login.password;
      } else {
        password = this.form.register.password;
      }

      if (!String(password).match(/^.{8,}$/)) {
        errorMessage = "Minimum of 8 characters required";
      } else {
        errorMessage = "";
      }

      if (type == "login") {
        this.errorMessage.login.password = errorMessage;
      } else {}

      this.errorMessage.register.password = errorMessage;
    },
    validatepassword_confirmation: function validatepassword_confirmation() {
      if (this.form.register.password != this.form.register.password_confirmation) {
        this.errorMessage.register.password_confirmation = "Password donot match";
      } else {
        this.errorMessage.register.password_confirmation = "";
      }
    }
  })
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/views/Login.vue?vue&type=template&id=12f5395a":
/*!**********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/views/Login.vue?vue&type=template&id=12f5395a ***!
  \**********************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

var _hoisted_1 = {
  "class": "bg1 pt-8"
};
var _hoisted_2 = {
  "class": "container"
};
var _hoisted_3 = {
  "class": "flip-container login"
};
var _hoisted_4 = {
  "class": "flipper",
  id: "flipper"
};
var _hoisted_5 = {
  "class": "front"
};
var _hoisted_6 = {
  "class": "panel border bg-white"
};

var _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  "class": "panel-heading"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  "class": "pt-3 font-weight-bold"
}, "Login")], -1
/* HOISTED */
);

var _hoisted_8 = {
  "class": "panel-body p-3"
};
var _hoisted_9 = {
  "class": "form-group py-2"
};
var _hoisted_10 = {
  "class": "input-field"
};

var _hoisted_11 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  "class": "far fa-user p-2"
}, null, -1
/* HOISTED */
);

var _hoisted_12 = {
  "class": "invalid-feedback d-block"
};
var _hoisted_13 = {
  "class": "form-group py-1 pb-2"
};
var _hoisted_14 = {
  "class": "input-field"
};

var _hoisted_15 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  "class": "fas fa-lock px-2"
}, null, -1
/* HOISTED */
);

var _hoisted_16 = ["type"];
var _hoisted_17 = {
  key: 0,
  "class": "far fa-eye-slash"
};
var _hoisted_18 = {
  key: 1,
  "class": "far fa-eye"
};
var _hoisted_19 = {
  "class": "invalid-feedback d-block"
};

var _hoisted_20 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  "class": "d-flex align-items-center view_btn mt-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
  type: "submit",
  "class": "btn btn-raised shadow my-button w-xs justify-content-center"
}, " Login ")], -1
/* HOISTED */
);

var _hoisted_21 = {
  "class": "text-center pt-4 text-muted"
};

var _hoisted_22 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Don't have an account? ");

var _hoisted_23 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createStaticVNode)("<div class=\"mx-3 my-2 py-2 bordert\"><div class=\"text-center py-3\"><a href=\"https://wwww.facebook.com\" target=\"_blank\" class=\"px-2\"><img src=\"https://www.dpreview.com/files/p/articles/4698742202/facebook.jpeg\" alt=\"\"></a><a href=\"https://www.google.com\" target=\"_blank\" class=\"px-2\"><img src=\"https://www.freepnglogos.com/uploads/google-logo-png/google-logo-png-suite-everything-you-need-know-about-google-newest-0.png\" alt=\"\"></a><a href=\"https://www.github.com\" target=\"_blank\" class=\"px-2\"><img src=\"https://www.freepnglogos.com/uploads/512x512-logo-png/512x512-logo-github-icon-35.png\" alt=\"\"></a></div></div>", 1);

var _hoisted_24 = {
  "class": "back"
};
var _hoisted_25 = {
  "class": "panel border bg-white"
};

var _hoisted_26 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  "class": "panel-heading"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  "class": "pt-3 font-weight-bold"
}, " Register ")], -1
/* HOISTED */
);

var _hoisted_27 = {
  "class": "panel-body p-3"
};
var _hoisted_28 = {
  "class": "form-group py-2"
};
var _hoisted_29 = {
  "class": "input-field"
};

var _hoisted_30 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  "class": "far fa-user p-2"
}, null, -1
/* HOISTED */
);

var _hoisted_31 = {
  "class": "invalid-feedback d-block"
};
var _hoisted_32 = {
  "class": "form-group py-2"
};
var _hoisted_33 = {
  "class": "input-field"
};

var _hoisted_34 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  "class": "far fa-user p-2"
}, null, -1
/* HOISTED */
);

var _hoisted_35 = {
  "class": "invalid-feedback d-block"
};
var _hoisted_36 = {
  "class": "form-group py-2"
};
var _hoisted_37 = {
  "class": "input-field"
};

var _hoisted_38 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  "class": "far fa-envelope p-2"
}, null, -1
/* HOISTED */
);

var _hoisted_39 = {
  "class": "invalid-feedback d-block"
};
var _hoisted_40 = {
  "class": "form-group py-1 pb-2"
};
var _hoisted_41 = {
  "class": "input-field"
};

var _hoisted_42 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  "class": "fas fa-lock px-2"
}, null, -1
/* HOISTED */
);

var _hoisted_43 = ["type"];
var _hoisted_44 = {
  key: 0,
  "class": "far fa-eye-slash"
};
var _hoisted_45 = {
  key: 1,
  "class": "far fa-eye"
};
var _hoisted_46 = {
  "class": "invalid-feedback d-block"
};
var _hoisted_47 = {
  "class": "form-group py-1 pb-2"
};
var _hoisted_48 = {
  "class": "input-field"
};

var _hoisted_49 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  "class": "fas fa-lock px-2"
}, null, -1
/* HOISTED */
);

var _hoisted_50 = ["type"];
var _hoisted_51 = {
  key: 0,
  "class": "far fa-eye-slash"
};
var _hoisted_52 = {
  key: 1,
  "class": "far fa-eye"
};
var _hoisted_53 = {
  "class": "invalid-feedback d-block"
};

var _hoisted_54 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  "class": "d-flex align-items-center view_btn mt-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
  type: "submit",
  "class": "btn btn-raised shadow my-button w-xs justify-content-center"
}, " Register ")], -1
/* HOISTED */
);

var _hoisted_55 = {
  "class": "text-center pt-4 text-muted"
};

var _hoisted_56 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Already have an account? ");

function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_Navbar = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("Navbar");

  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" navbar "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_Navbar), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("section", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [_hoisted_7, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("form", {
    onSubmit: _cache[6] || (_cache[6] = (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)(function () {
      return $options.loginUser && $options.loginUser.apply($options, arguments);
    }, ["prevent"]))
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_10, [_hoisted_11, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "text",
    placeholder: "Email",
    required: "",
    "onUpdate:modelValue": _cache[0] || (_cache[0] = function ($event) {
      return $data.form.login.email = $event;
    }),
    onInput: _cache[1] || (_cache[1] = function ($event) {
      return $options.validateEmail('login');
    })
  }, null, 544
  /* HYDRATE_EVENTS, NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.form.login.email]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("small", _hoisted_12, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.errorMessage.login.email), 1
  /* TEXT */
  )]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_13, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [_hoisted_15, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: $data.hideLoginPassword ? 'password' : 'text',
    placeholder: "Enter your Password",
    required: "",
    "onUpdate:modelValue": _cache[2] || (_cache[2] = function ($event) {
      return $data.form.login.password = $event;
    }),
    onInput: _cache[3] || (_cache[3] = function ($event) {
      return $options.validatePassword('login');
    })
  }, null, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_16), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelDynamic, $data.form.login.password]]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
    "class": "btn bg-white text-muted",
    type: "button",
    onClick: _cache[4] || (_cache[4] = function ($event) {
      return $data.hideLoginPassword = !$data.hideLoginPassword;
    })
  }, [$data.hideLoginPassword ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_17)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_18))])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("small", _hoisted_19, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.errorMessage.login.password), 1
  /* TEXT */
  )]), _hoisted_20, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_21, [_hoisted_22, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("a", {
    onClick: _cache[5] || (_cache[5] = function () {
      return $options.flipForm && $options.flipForm.apply($options, arguments);
    }),
    "class": "flipbutton",
    id: "loginButton",
    href: "#"
  }, "Sign up")])], 32
  /* HYDRATE_EVENTS */
  )]), _hoisted_23])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_24, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_25, [_hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_27, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("form", {
    onSubmit: _cache[18] || (_cache[18] = (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)(function () {
      return $options.registerUser && $options.registerUser.apply($options, arguments);
    }, ["prevent"]))
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_28, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_29, [_hoisted_30, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "text",
    placeholder: "First Name",
    required: "",
    "onUpdate:modelValue": _cache[7] || (_cache[7] = function ($event) {
      return $data.form.register.firstName = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.form.register.firstName]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("small", _hoisted_31, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.errorMessage.register.firstName), 1
  /* TEXT */
  )]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_32, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_33, [_hoisted_34, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "text",
    placeholder: "Last Name",
    required: "",
    "onUpdate:modelValue": _cache[8] || (_cache[8] = function ($event) {
      return $data.form.register.lastName = $event;
    })
  }, null, 512
  /* NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.form.register.lastName]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("small", _hoisted_35, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.errorMessage.register.lastName), 1
  /* TEXT */
  )]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_36, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_37, [_hoisted_38, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "text",
    placeholder: "Email Address",
    required: "",
    "onUpdate:modelValue": _cache[9] || (_cache[9] = function ($event) {
      return $data.form.register.email = $event;
    }),
    onInput: _cache[10] || (_cache[10] = function ($event) {
      return $options.validateEmail('register');
    })
  }, null, 544
  /* HYDRATE_EVENTS, NEED_PATCH */
  ), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $data.form.register.email]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("small", _hoisted_39, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.errorMessage.register.email), 1
  /* TEXT */
  )]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_40, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_41, [_hoisted_42, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: $data.hideRegisterPassword ? 'password' : 'text',
    placeholder: "Enter your Password",
    required: "",
    "onUpdate:modelValue": _cache[11] || (_cache[11] = function ($event) {
      return $data.form.register.password = $event;
    }),
    onInput: _cache[12] || (_cache[12] = function ($event) {
      return $options.validatePassword('register');
    })
  }, null, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_43), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelDynamic, $data.form.register.password]]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
    "class": "btn bg-white text-muted",
    type: "button",
    onClick: _cache[13] || (_cache[13] = function ($event) {
      return $data.hideRegisterPassword = !$data.hideRegisterPassword;
    })
  }, [$data.hideRegisterPassword ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_44)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_45))])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("small", _hoisted_46, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.errorMessage.register.password), 1
  /* TEXT */
  )]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_47, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_48, [_hoisted_49, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: $data.hideRegisterpassword_confirmation ? 'password' : 'text',
    placeholder: "Enter Confirm Password",
    required: "",
    "onUpdate:modelValue": _cache[14] || (_cache[14] = function ($event) {
      return $data.form.register.password_confirmation = $event;
    }),
    onInput: _cache[15] || (_cache[15] = function () {
      return $options.validatepassword_confirmation && $options.validatepassword_confirmation.apply($options, arguments);
    })
  }, null, 40
  /* PROPS, HYDRATE_EVENTS */
  , _hoisted_50), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelDynamic, $data.form.register.password_confirmation]]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
    "class": "btn bg-white text-muted",
    type: "button",
    onClick: _cache[16] || (_cache[16] = function ($event) {
      return $data.hideRegisterpassword_confirmation = !$data.hideRegisterpassword_confirmation;
    })
  }, [$data.hideRegisterpassword_confirmation ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_51)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_52))])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("small", _hoisted_53, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($data.errorMessage.register.password_confirmation), 1
  /* TEXT */
  )]), _hoisted_54, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_55, [_hoisted_56, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("a", {
    onClick: _cache[17] || (_cache[17] = function () {
      return $options.flipForm && $options.flipForm.apply($options, arguments);
    }),
    "class": "flipbutton",
    id: "registerButton",
    href: "#"
  }, "Sign in")])], 32
  /* HYDRATE_EVENTS */
  )])])])])])])])]);
}

/***/ }),

/***/ "./resources/js/views/Login.vue":
/*!**************************************!*\
  !*** ./resources/js/views/Login.vue ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Login_vue_vue_type_template_id_12f5395a__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=12f5395a */ "./resources/js/views/Login.vue?vue&type=template&id=12f5395a");
/* harmony import */ var _Login_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js */ "./resources/js/views/Login.vue?vue&type=script&lang=js");
/* harmony import */ var C_laragon_www_food_ordering_system_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,C_laragon_www_food_ordering_system_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_Login_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_Login_vue_vue_type_template_id_12f5395a__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"resources/js/views/Login.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (__exports__);

/***/ }),

/***/ "./resources/js/views/Login.vue?vue&type=script&lang=js":
/*!**************************************************************!*\
  !*** ./resources/js/views/Login.vue?vue&type=script&lang=js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Login_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"])
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Login_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./Login.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/views/Login.vue?vue&type=script&lang=js");
 

/***/ }),

/***/ "./resources/js/views/Login.vue?vue&type=template&id=12f5395a":
/*!********************************************************************!*\
  !*** ./resources/js/views/Login.vue?vue&type=template&id=12f5395a ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Login_vue_vue_type_template_id_12f5395a__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Login_vue_vue_type_template_id_12f5395a__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./Login.vue?vue&type=template&id=12f5395a */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/views/Login.vue?vue&type=template&id=12f5395a");


/***/ })

}]);