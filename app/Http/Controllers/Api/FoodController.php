<?php

namespace App\Http\Controllers\Api;

use App\Models\Food;
use Orion\Http\Controllers\Controller;
use Orion\Concerns\DisableAuthorization;

class FoodController extends Controller
{
    use DisableAuthorization;

    protected $model = Food::class;

    /* fetch foods by category  */

    public function foodsByCategory($category)
    {
        $foods = Food::with('category')->where('category_id', $category)->get();

        return response()->json($foods);
    }
}
