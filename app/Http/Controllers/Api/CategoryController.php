<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Orion\Http\Controllers\Controller;
use Orion\Concerns\DisableAuthorization;

class CategoryController extends Controller
{
    use DisableAuthorization;

    protected $model = Category::class;

    public function homeCategories()
    {
        $categories = Category::all();

        return response()->json($categories);
    }
}
