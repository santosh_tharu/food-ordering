<?php

use Orion\Facades\Orion;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\FoodController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Api\CategoryController;

//public routes

Route::middleware(['guest:api'])->group(function () {
    Route::get('/', function () {
        return view('app');
    });
    //register and login
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);


    //home category

    Route::get('category/home', [CategoryController::class, 'homeCategories'])->name('home-categories');
    Route::get('category/{category}/foods', [FoodController::class, 'foodsByCategory'])->name('category-foods');
});
Route::middleware(['auth:api', 'verified'])->group(function () {
    //profile info
    Route::get('profile', [AuthController::class, 'profile']);
    Route::get('logout', [AuthController::class, 'logout']);

    /* crud apis */
    Route::group(['as' => 'api.'], function () {
        Orion::resource('categories', CategoryController::class);
        Orion::resource('foods', FoodController::class);
    });
});
